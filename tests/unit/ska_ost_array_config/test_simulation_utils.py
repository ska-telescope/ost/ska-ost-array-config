import pickle
from pathlib import Path

import astropy.time
import pytest
from astropy import units
from astropy.coordinates import SkyCoord
from astropy.time import Time

from ska_ost_array_config.array_config import MID_ARRAY_REF, LowSubArray
from ska_ost_array_config.simulation_utils import (
    find_rise_set_times,
    simulate_observation,
)

TOLERANCE = 1e-3


def test_simulation(test_observation):
    """
    Test ska_ost_array_config.simulation_utils.simulate_observation()
    """
    pickle_file = (
        Path(__file__).resolve().parent.parent / "static/mid_observation.pickle"
    )
    with open(pickle_file, "rb") as f:
        expected_obs = pickle.load(f)
    assert (test_observation.time.data - expected_obs.time.data < TOLERANCE).all()
    assert (
        test_observation.frequency.data - expected_obs.frequency.data < TOLERANCE
    ).all()
    assert (
        test_observation.integration_time.data - expected_obs.integration_time.data
        < TOLERANCE
    ).all()
    assert (test_observation.uvw.data - expected_obs.uvw.data < TOLERANCE).all()
    assert (
        test_observation.channel_bandwidth.data - expected_obs.channel_bandwidth.data
        < TOLERANCE
    ).all()
    assert test_observation.phasecentre == expected_obs.phasecentre


def test_simulate_observation_invalid_start_time():
    """
    simulate_observation() should throw a TypeError if start_time is neither a float
    nor a valid astropy Time object"""
    low_aa2 = LowSubArray(subarray_type="AA2")

    # Target coordinate - Polaris Australis
    phase_centre = SkyCoord("21:08:46.8 -88:57:23.4", unit=(units.hourangle, units.deg))

    with pytest.raises(TypeError):
        simulate_observation(
            array_config=low_aa2.array_config,
            phase_centre=phase_centre,
            start_time="a",
            duration=12 * 3600.0,
            integration_time=60,
            ref_freq=1420e6,
            chan_width=13.4e3,
            n_chan=1,
            horizon=20,
        )


def test_times_neverup():
    """Source at 08:00:00 50:00:00 should never be visible from MID if the horizon is set to 15 deg"""
    phase_centre = SkyCoord("08:00:00 50:00:00", unit=(units.hourangle, units.deg))
    obs_date = Time.now()
    result = find_rise_set_times(
        MID_ARRAY_REF, phase_centre, obs_date, elevation_limit=15.0
    )
    assert result == (None, None, None)


def test_times_always_up():
    """Source at 04:00:00 -75:00:00 should always be visible from MID"""
    phase_centre = SkyCoord("04:00:00 -75:00:00", unit=(units.hourangle, units.deg))
    obs_date = Time.now()
    result = find_rise_set_times(
        MID_ARRAY_REF, phase_centre, obs_date, elevation_limit=15.0
    )
    # We don't really care about the exact transit time. We just want to check that it is not None
    assert (
        result[0] is None
        and isinstance(result[1], astropy.time.Time)
        and result[2] is None
    )
