###########
Change Log
###########

Unreleased
-----------
* Upgrade dependency versions

3.1.0
-----------
* Update Low AA2 after ECP-240228 (see OPS-270)
* Add Low AA* phase 1 as defined in ECP-240228

3.0.1
-----------
* Temporary fix for the multichannel gridding issue (see OPS-266)

3.0.0
-----------
* Remove time and frequency undersample parameters from simulate_observation()
* Fix incorrect end_ha calculation in simulate_observation() (see OPS-260 for more information)
* Add warning in LowSubArray and MidSubArray if an invalid custom_station is specified (OPS-262)
* Allow users to add/remove dishes/stations from subarray templates

2.3.1
----------
* Fix OPS-258 (allow users to specify observation start times in hour angle)

2.3.0
----------
* Fix OPS-259 (allow users to add stations/dishes to array_assemblies)

2.2.1
----------
* Fix OPS-261

2.2.0
----------
* Add get_mid_dish_coordinates() to ska_ost_array_config.array_config
* Update array_assembly.py to incorporate changes made to Low array layout by ECP-240120
* Add generate_miriad_antenna_list() to array_config.SubArray class

2.1.0
-----
* Add the subarray templates library (corresponds to memo revision 01)

2.0.0
-----
* Second public release of the staged delivery and array assemblies memo (revision 02).

1.0.5
----------
* Fix bug encountered in 1.0.4 (see OPS-253 for more information)
* Verified to work with py3.10/3.11

1.0.4 (Do not use this version.)
----------
* Update Mid AA2 and MK+ layout following the release of SKA-TEL-SKO-0001827 revision 02.
* Update Low coordinates to include station HAE
* Add get_low_station_rotation() and get_low_station_coordinates() to ska_ost_array_config.array_config

1.0.3
-----
* Ensure proper time-tagging in simulated observations. 

1.0.2
-----
* Verified to work with py3.9/3.10/3.11
* Add plot_collecting_area_vs_bl_length() to SubArray class.

1.0.1
-----
* Update Mid coordinates to version 10.
* Reformat coordinate files to csv

1.0.0
-----
* First public release of the staged delivery and array assemblies memo along with ska_ost_array_config package. 
